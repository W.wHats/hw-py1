def sum_range(a, b):
    sum = 0
    if a > b:
        a, b = b, a
    for i in range(a, b + 1):
        sum += i
    return sum


print(sum_range(2, 15))
